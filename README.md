# Flask data-pipeline WebApp

### Install
  - pip install -r requirements.txt

### Required ENV vars
  - SQLALCHEMY_DATABASE_URL
    - export SQLALCHEMY_DATABASE_URL=postgresql+psycopg2://dev:password@aurora-cluster-dev-datapipeline.cluster-123.us-east-1.rds.amazonaws.com/DBNAME
  - FLASK_APP
    - export FLASK_APP="flask-pipelines.app:app"

### To run developement server:
  - `flask run --reload --port 8080 --host 0.0.0.0`
    - helper sh script `run.sh` will run the above.

