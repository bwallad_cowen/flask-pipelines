import os

from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker


SQLALCHEMY_DATABASE_URL = os.getenv("SQLALCHEMY_DATABASE_URL", None)
if SQLALCHEMY_DATABASE_URL is None:
    raise Exception("env var SQLALCHEMY_DATABASE_URL is not defined!")

engine = create_engine(
    SQLALCHEMY_DATABASE_URL, connect_args={} # may need this.
)

Session = sessionmaker(autocommit=False, autoflush=False, bind=engine)
