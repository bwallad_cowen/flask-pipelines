from sqlalchemy.sql.expression import column, outerjoin, select, func
from sqlalchemy.dialects.postgresql import INTERVAL
from data_eng_lib.event_metadata import meta as EM
from sqlalchemy.sql.sqltypes import INT


tbl_schedule = EM.scheduled_event
tbl_event = EM.event_type
tbl_pe = EM.pipeline_event


def get_scheduled_events_query_series(current_datetime, interval):
    if current_datetime is None:
        current_datetime = func.now()

    _interval = func.cast(interval, INTERVAL)

    # generates a series of N dates to display from the current datetime.
    date_series = func.generate_series(
        current_datetime - _interval,
        current_datetime + _interval,
        '1 day'
    ).alias('date_series')

    dates_cte = (
        select(func.date_trunc('day', column('date_series')).label('dt'))
        .select_from(date_series)
    ).cte('dates_cte')

    query_cols = [
        (dates_cte.c.dt + func.cast('5 hour', INTERVAL)).label('dt'),
        tbl_event.c.event_type,
        tbl_schedule.c.expires_date_time,
        tbl_schedule.c.actual_completed_at_datetime,
        tbl_pe.c.pipeline_run_id,
    ]
    query = (
        select(*query_cols)
        .outerjoin(tbl_schedule, dates_cte.c.dt == tbl_schedule.c.estimated_scheduled_date_time)
        .outerjoin(tbl_event, tbl_event.c.event_type_id == tbl_schedule.c.event_type_id)
        .outerjoin(tbl_pe, tbl_pe.c.occured_event_id == tbl_schedule.c.occured_event_id)
    ).order_by(dates_cte.c.dt)
    return query


def get_scheduled_events_query(current_datetime, interval):
    if current_datetime is None:
        current_datetime = func.now()

    _interval = func.cast(interval, INTERVAL)

    start_dt = current_datetime - _interval
    end_dt = current_datetime + _interval

    query_cols = [
        tbl_event.c.event_type,
        tbl_schedule.c.estimated_scheduled_date_time.label("dt"),
        tbl_schedule.c.occured_event_id.isnot(None).label("occured"),
        tbl_schedule.c.actual_completed_at_datetime,
        tbl_pe.c.pipeline_run_id,
    ]

    where_clause = [
        tbl_schedule.c.estimated_scheduled_date_time >= start_dt,
        tbl_schedule.c.estimated_scheduled_date_time <= end_dt,
    ]

    query = (
        select(*query_cols)
        .join(tbl_schedule, tbl_schedule.c.event_type_id == tbl_event.c.event_type_id)
        .outerjoin(tbl_pe, tbl_pe.c.occured_event_id == tbl_schedule.c.occured_event_id)
        .where(*where_clause)
    )
    return query

