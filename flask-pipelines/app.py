from flask import (
    Flask, _app_ctx_stack, jsonify, url_for, render_template, request
)
from sqlalchemy.sql.schema import DEFAULT_NAMING_CONVENTION
from flask_cors import CORS

from sqlalchemy.orm import scoped_session

from .db import Session
from .queries import get_scheduled_events_query


def _jsonify(records):
    return jsonify([*map(lambda x: dict(x), records)])


app = Flask(__name__)
CORS(app)
app.session = scoped_session(Session, scopefunc=_app_ctx_stack.__ident_func__)
DEFAULT_INTERVAL = 14


def _get_event_params():
    current_datetime = request.args.get("current_datetime")
    interval = request.args.get("interval", DEFAULT_INTERVAL)
    interval = f"{interval} day"
    return current_datetime, interval


@app.route("/")
def home():
    current_datetime, interval = _get_event_params()
    q = get_scheduled_events_query(current_datetime, interval)
    events = app.session.execute(q).fetchall()
    return render_template("calendar.html", events=events)


@app.route("/events/")
def events():
    current_datetime, interval = _get_event_params()

    query = get_scheduled_events_query(current_datetime, interval)
    records = app.session.execute(query).fetchall()
    return _jsonify(records)


@app.teardown_appcontext
def remove_session(*args, **kwargs):
    app.session.remove()
